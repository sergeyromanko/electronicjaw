﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ShapeDescription))]
public class ShapeDescriptionDrawer : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var amountRect = new Rect(position.x, position.y, position.width*0.5f - 10f, position.height);
        var unitRect   = new Rect(position.x + amountRect.width + 10f, position.y, position.width*0.25f - 5f, position.height);
        var nameRect   = new Rect(unitRect.x + unitRect.width + 5f, position.y, position.width*0.25f, position.height);
        
        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("sprite"), GUIContent.none);
        EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("points"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("chance"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}