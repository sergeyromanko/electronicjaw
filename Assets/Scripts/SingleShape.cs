using UnityEngine;
using UnityEngine.UI;

public class SingleShape : MonoBehaviour {
    
    private static readonly int LifetimeParameter = Animator.StringToHash("lifetime");
    
    public  int      points;
    public  Color    color;
    public  Animator animator;
    private float    lifetime = 1f;
    
    void Update() {
        lifetime -= Time.deltaTime;
        
        animator.SetFloat(LifetimeParameter, lifetime);
        
        if (lifetime <= 0) Destroy(gameObject);
    }
    
}
