using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Bootstrap : MonoBehaviour {

    public GameObject camera;
    public GameObject eventSystem;
    
    private void Start() {
        DontDestroyOnLoad(camera);
        DontDestroyOnLoad(eventSystem);
        
        StartCoroutine(GetRequest("http://www.google.com"));
    }
    
    public IEnumerator GetRequest(string uri) {
        var unityWebRequest = UnityWebRequest.Get(uri);
        yield return unityWebRequest.SendWebRequest();

        if (unityWebRequest.isNetworkError) {
            Debug.Log("Error While Sending: " + unityWebRequest.error);
        } else {
            Debug.Log("Received: " + unityWebRequest.downloadHandler.text);
        }
        
        StartGame();
    }

    public void StartGame() {
        SceneManager.LoadScene("Main Menu");
    }
    
}