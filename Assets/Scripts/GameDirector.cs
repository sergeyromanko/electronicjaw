﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Assert = UnityEngine.Assertions.Assert;
using Random = UnityEngine.Random;

[Serializable]
public class ShapeDescription { 
    public Sprite sprite;
    public int    points;
    public float  chance;
}

public class GameDirector : MonoBehaviour  {

    [HideInInspector] public int score;
    [HideInInspector] public float timer = 60f;
    [HideInInspector] public float spawnTimer = 0f;

    public ShapeDescription[] shapes;
    public GameObject         shapePrefab;
    public GameObject         popupPrefab;
    public Color[]            positiveColors;
    public Color[]            negativeColors;

    private Camera mainCamera;

    private void Start() {
        Assert.IsTrue(shapes.Select(x=>x.chance).Sum() == 1, "Chances sum is less than 1.");
        mainCamera = Camera.main;
    }

    void Update() {
        
        timer = Mathf.Max(timer - Time.deltaTime, 0);

        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0) {
            spawnTimer = 1f + Random.Range(-0.30f, + 0.15f);
            SpawnShape();
        }

        if (Input.GetMouseButtonDown(0)) {
            var wp = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            var collider = Physics2D.OverlapPoint(wp);
            if (collider != null){
                var shapeComponent = collider.gameObject.GetComponent<SingleShape>();
                score += shapeComponent.points;
                
                var popupObject = Instantiate(popupPrefab, shapeComponent.transform.position, Quaternion.identity, transform);
                popupObject.GetComponentInChildren<TextMesh>().text = (shapeComponent.points > 0 ? "+" : "") + shapeComponent.points;
                popupObject.GetComponentInChildren<TextMesh>().color = shapeComponent.color;
                
                Destroy(shapeComponent.gameObject);
            }
        }
        
    }
    
    private void OnDrawGizmos() {
        var rect = GetActivePartOfScreen(Camera.main);
        Gizmos.DrawWireCube(rect.center, rect.size);
    }
    
    private void SpawnShape() {
        var curChance = Random.Range(0.0f, 1.0f);
        var curIndex = 0;
        var accumulator = 0f;
        for (int i = 0; i < shapes.Length; i++) {
            accumulator += shapes[i].chance;
            if (curChance <= accumulator) {
                curIndex = i;
                break;
            }
        }

        var isPositive = Random.Range(0f, 1f) > 0.25f;

        var colorArray = isPositive ? positiveColors : negativeColors;
        var color = colorArray[Random.Range(0, colorArray.Length)];
        var rect = GetActivePartOfScreen(mainCamera);
        var position = new Vector3( Random.Range(rect.xMin, rect.xMax),  Random.Range(rect.yMin, rect.yMax), 0);
        var shapeObject = Instantiate(shapePrefab, position, Quaternion.identity, transform);
        shapeObject.GetComponent<SpriteRenderer>().sprite = shapes[curIndex].sprite;
        shapeObject.GetComponent<SpriteRenderer>().color  = color;
        shapeObject.GetComponent<SingleShape>().points    = shapes[curIndex].points * (isPositive ? +1 : -1);
        shapeObject.GetComponent<SingleShape>().color     = color;
    }


    public void Reset() {
        timer = 60;
        score = 0;
        spawnTimer = 0;
        foreach (Transform child in transform) {
            Destroy(child.gameObject);
        }
    }

    private Rect GetActivePartOfScreen(Camera mainCamera) {
        var min = mainCamera.ViewportToWorldPoint(new Vector3(0f, 0f, 0f));
        var max = mainCamera.ViewportToWorldPoint(new Vector3(1f, 1f, 0f));
        return Rect.MinMaxRect(min.x + 1f, min.y + 1f , max.x - 1f, max.y - 1.5f);
    }
    
}
