using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverView : MonoBehaviour {

    public GameDirector gameDirector;

    public GameObject gameOverUi;

    void Update() {
        if (gameDirector.timer <= 0 && gameOverUi.activeSelf == false) {
            gameOverUi.SetActive(true);
            Time.timeScale = 0f;
        }
    }
    
    public void OnMainMenuClick() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Main Menu");
    }
    
    public void OnPlayAgainClick() {
        gameOverUi.SetActive(false);
        gameDirector.Reset();
        Time.timeScale = 1f;
    }
    
}