using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseView : MonoBehaviour {

    public void OnContinue() {
        Time.timeScale = 1f;
        gameObject.SetActive(false);
    }
    
    public void OnGoToMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Main Menu");
    }
    
}