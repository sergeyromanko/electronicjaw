using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuView : MonoBehaviour {
    
    public void OnPlayClick() {
        SceneManager.LoadScene("Gameplay");
    }
    
}