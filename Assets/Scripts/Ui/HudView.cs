﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class HudView : MonoBehaviour {

    public GameDirector gameDirector;
    public GameObject   pauseView;

    public Text  scoreText;
    public Text  timerText;
    public Image timerImage;

    void Update() {
        scoreText.text = gameDirector.score.ToString();
        var time = Mathf.Ceil(gameDirector.timer).ToString();
        timerText.text = (time.Length == 1 ? "0" : "") + time;

        var color = gameDirector.timer < 5f ? Color.red : Color.white;
        timerText.color = color;
        timerImage.color = color;
    }

    public void PauseClick() {
        Time.timeScale = (Time.timeScale == 1f) ? 0f : 1f;
        if(Time.timeScale == 0f) pauseView.SetActive(true);
    }
    
}
